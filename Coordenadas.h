#include <list>
#include <iostream>
using namespace std;


class Coordenada{
	private:
		float X=0;
		float Y=0;
		float Z=0;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public:
	//constructores::
		Coordenada();
		Coordenada(float X, float Y, float Z);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//metodos set y get::
		float get_X();
		float get_Y();
		float get_Z();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		void set_X(float X);
		void set_Y(float Y);
		void set_Z(float Z);
};
	
