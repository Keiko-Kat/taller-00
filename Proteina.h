#include <list>
#include <iostream>
using namespace std;

class Proteina{
	private:
		string nombre = "\0";
		string id = "\0";
		list<Cadena> cadenas;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public:
	//constructores::
		Proteina();
		Proteina(string id, string nombre);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//metodos set y get::
		void set_nombre(string nombre);
		void set_id(string id);
		void add_cadenas(Cadena cadenas);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		string get_nombre();
		string get_id();
		Cadena get_cadenas();
};
