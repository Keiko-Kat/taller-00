#include <list>
#include <iostream>
using namespace std;

class Aminoacido{
	private:
		string nombre = "\0";
		int numero = 0;
		Atomo atomos = list<Atomo>;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public:
	//constructores::
		Aminoacido();
		Aminoacido(string nombre, int numero);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//metodos set y get::
		void set_nombre(string nombre);
		void set_numero(int numero);
		void add_atomo(Atomo atomo);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		string get_nombre();
		int get_numero();
		Atomo get_atomos();
};
