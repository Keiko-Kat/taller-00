#include <list>
#include <iostream>
using namespace std;

class Atomo{
	private:
		string nombre = "\0";
		int numero = 0;
		Coordenada coordenada = new Coordenada();
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public:
	//constructores::
		Atomo();
		Atomo(string nombre, int numero);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//metodos set y get::
		void set_nombre(string nombre);
		void set_numero(int numero);
		void set_coordenada(Coordenada coordenada)
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		string get_nombre();
		int get_numero();
		Coordenada get_coordenada();
};
		
