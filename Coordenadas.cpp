#include <list>
#include <iostream>
using namespace std;
#include "Coordenadas.h"


Coordenada::Coordenada(){
	float X=0;
	float Y=0;
	float Z=0;
}

Coordenada::Coordenada(float X, float Y, float Z){
	this->X = X;
	this->Y = Y;
	this->Z = Z;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//metodos set y get::
float Coordenada::get_X(){
	return this->X;
}
float Coordenada::get_Y(){
	return this->Y;
}
float Coordenada::get_Z(){
	return this->Z;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void Coordenada::set_X(float X){
	this->X = X;
}
void Coordenada::set_Y(float Y){
	this->Y = Y;
}
void Coordenada::set_Z(float Z){
	this->Z = Z;
}
