#include <list>
#include <iostream>
using namespace std;

class Cadena{
	private:
		string letra = "\0";
		list<Aminoacido> aminoacidos;
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	public:
	//constructores::
		Cadena();
		Cadena(string letra);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	//metodos set y get::
		void set_letra(string letra);
		void add_aminoacidos(Aminoacido aminoacido);
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
		string get_letra();
		Aminoacido get_aminoacidos();
};
